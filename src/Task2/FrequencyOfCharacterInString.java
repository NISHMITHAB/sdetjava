package Task2;

public class FrequencyOfCharacterInString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "This is selenium and java training.";
        char ch = 'i';
        int freq = 0;

        for(int i = 0; i < str.length(); i++) {
            if(ch == str.charAt(i)) {
                ++freq;
            }
        }

        System.out.println("Frequency of " + ch + " is " + freq);
    
	}

}
