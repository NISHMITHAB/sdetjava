package Task2;

public class Time {

	
		// TODO Auto-generated method stub
		int sec;
	    int min;
	    int hr;

	    public Time(int hours, int minutes, int seconds) {
	        this.hr = hours;
	        this.min = minutes;
	        this.sec = seconds;
	    }

	    public static void main(String[] args) {
	        Time start = new Time(12, 34, 55),
	                stop = new Time(8, 12, 15),
	                diff;

	        diff = difference(start, stop);

	        System.out.printf("TIME DIFFERENCE: %d:%d:%d - ", start.hr, start.min, start.sec);
	        System.out.printf("%d:%d:%d ", stop.hr, stop.min, stop.sec);
	        System.out.printf("= %d:%d:%d\n", diff.hr, diff.min, diff.sec);
	    }

	    public static Time difference(Time start, Time stop)
	    {
	        Time diff = new Time(0, 0, 0);

	        if(stop.sec > start.sec){
	            --start.min;
	            start.sec += 60;
	        }

	        diff.sec = start.sec - stop.sec;
	        if(stop.min > start.min){
	            --start.hr;
	            start.min += 60;
	        }

	        diff.min = start.min - stop.min;
	        diff.hr = start.hr - stop.hr;

	        return(diff);
	    }
	}


