package Task2;

public class ConcateTwoArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr1 = {1, 2, 3};
        int[] arr2 = {4, 5, 6};

        int len1 = arr1.length;
        int len2 = arr2.length;
        int[] res= new int[len1+len2];
        int k=0;
        for(int i=0;i<len1;i++) {
        	res[k]=arr1[i];
        	k++;
        }
        for(int j=0;j<len2;j++) {
        	res[k]=arr2[j];
        	k++;
        }
        System.out.println("Final array is ");
        for(int i=0;i<k;i++) {
        	System.out.print(res[i]+" ");
        }
	}

}
