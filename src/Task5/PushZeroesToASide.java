package Task5;

import java.util.Scanner;

public class PushZeroesToASide {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int count=0;
		int[] arr= {0,0,1,0,7,2,0,3,9,0,11};
		int n=arr.length;
		 for (int i = 0; i < n; i++) {
	            if (arr[i] != 0) {
	                arr[count] = arr[i];  
	                count++;
	            }
		 }
	        while (count < n) {
	            arr[count++] = 0; 
	        }
	        for(int i=0;i<count;i++) {
	        	System.out.print(arr[i]+" ");
	        }
	}

}
