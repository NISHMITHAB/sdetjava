package Task5;

import java.util.Scanner;

public class BinaryNumOrNot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num,rem;
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the number");
		num=sc.nextInt();
		int flag=0;
		while(num>0) {
			rem=num%10;
			if(rem!=0 && rem!=1) {
				flag++;
			}
			num=num/10;
		}
		if(flag==0) {
			System.out.println(" a binary number");
		}else {
			System.out.println("not a binary number");
		}
	}

}
