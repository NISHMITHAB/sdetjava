package Task5;

import java.util.Scanner;

public class SecondLargest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Enter the number of elements");
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		int[] a=new int[num];
		System.out.println("Enter the array elements");
		for(int i=0;i<num;i++) {
			a[i]=sc.nextInt();
		}
		int large=0,seclarge=0;
		for(int i=0;i<num;i++) {
			if(a[i]>large || a[i]<seclarge) {
				large=a[i];
				seclarge=a[i];
			}
		}
		System.out.println("second largest is = "+seclarge);
	}

}
