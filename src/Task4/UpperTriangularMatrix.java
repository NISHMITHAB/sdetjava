package Task4;

import java.util.Scanner;

public class UpperTriangularMatrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] a=new int[10][10];
		int  rows = 0, cols = 0;
		Scanner sc=new Scanner(System.in);
		System.out.println ("Enter the order of the matrix (mxn):\n");
		rows=sc.nextInt();
		cols=sc.nextInt();   
		System.out.println ("Enter the elements of the matrix\n");
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				a[i][j]=sc.nextInt();
			}
		}        
	   if(rows != cols){    
	              System.out.println("Matrix should be a square matrix");    
	  }    
	  else {    
	                 
	       System.out.println("Lower triangular matrix: ");    
	       for(int i = 0; i < rows; i++){    
	         for(int j = 0; j < cols; j++){    
	             if(j < i)    {
	                  System.out.print("0 ");    
	             }else {    
	                      System.out.print(a[i][j] + " "); 
	             }
	            }    
	           System.out.println();    
	         }    
	        }
	}

}
