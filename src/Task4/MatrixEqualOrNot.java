package Task4;

import java.util.Scanner;

public class MatrixEqualOrNot {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] a=new int[10][10];
		int[][] b=new int[10][10];
		int flag=0;
		Scanner sc=new Scanner(System.in);
		System.out.println ("Enter the order of the matrix A(mxn):\n");
		int arow=sc.nextInt();
		int acol=sc.nextInt();
		System.out.println ("Enter the elements of the A matrix\n");
		for (int i = 0; i < arow; i++)
		{
			for (int j = 0; j < acol; j++)
			{
				a[i][j]=sc.nextInt();
			}
		}
		
		System.out.println ("Enter the order of the matrix B(mxn):\n");
		int brow=sc.nextInt();
		int bcol=sc.nextInt();
		System.out.println ("Enter the elements of the A matrix\n");
		for (int i = 0; i < brow; i++)
		{
			for (int j = 0; j < bcol; j++)
			{
				b[i][j]=sc.nextInt();
			}
		}
		if(arow!=brow || acol!=bcol) {
			
			System.out.println (" not equal");
			
		}else {
			for(int i=0,j=0; i<arow && j<brow;i++,j++) {
				for(int k=0,l=0; k<acol && l<bcol;k++,l++) {
					if(a[i][k]!=b[j][l]) {
						flag++;
					}
				}
			}
			
		}
		if(flag>0) {

			System.out.println (" not equal");
		}else {

			System.out.println ("  equal");
		}
	}

}
