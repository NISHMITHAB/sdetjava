package Task4;

import java.util.Scanner;

public class IdentityMatrixOrNot {
	public static void main(String[] args) {
		int[][] a=new int[10][10];
		int i = 0, j = 0, row = 0, col = 0;
		Scanner sc=new Scanner(System.in);
		do {
		System.out.println ("Enter the order of the matrix (mxn):\n");
		row=sc.nextInt();
		col=sc.nextInt();
		}while(row!=col);
		int flag = 0;
	 
		System.out.println ("Enter the elements of the matrix\n");
		for (i = 0; i < row; i++)
		{
			for (j = 0; j < col; j++)
			{
				a[i][j]=sc.nextInt();
			}
		}
	 
		for (i = 0; i < row; i++)
		{
			for (j = 0; j < col; j++)
			{
				if (i == j && a[i][j] != 1)
				{
					flag = -1;
					break;
				}
				else if (i != j && a[i][j] != 0)
				{
					flag = -1;
					break;
				}
			}
		}
	 
		if (flag == 0)
		{
			System.out.println ("Identity Matrix");
		}
		else
		{
			System.out.println ("Not an identity matrix");
		}
	 
	}
}
