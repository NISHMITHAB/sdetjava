package Task4;

import java.util.Scanner;

public class RowAndColumnSum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] a=new int[10][10];
		int i = 0, j = 0, row = 0, col = 0,rsum=0,csum=0;
		Scanner sc=new Scanner(System.in);
		System.out.println ("Enter the order of the matrix (mxn):\n");
		row=sc.nextInt();
		col=sc.nextInt();
		System.out.println ("Enter the elements of the matrix\n");
		for (i = 0; i < row; i++)
		{
			for (j = 0; j < col; j++)
			{
				a[i][j]=sc.nextInt();
			}
		}
		for( i=0;i<row;i++) {
			rsum=0;
			for(j=0;j<col;j++) {
				rsum=rsum+a[i][j];
			}
			System.out.println ("Row sum is"+ rsum);
		}
		for( i=0;i<col;i++) {
			csum=0;
			for(j=0;j<row;j++) {
				csum=csum+a[j][i];
			}
			System.out.println ("Col sum is"+ csum);
		}
	}

}
