package Task4;

import java.util.Scanner;

public class OddEvenInMatrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		int[][] a=new int[10][10];
		int row,col,odd=0,even=0;
		System.out.println ("Enter the order of the matrix (mxn):\n");
		row=sc.nextInt();
		col=sc.nextInt();
		System.out.println ("Enter the elements of the matrix\n");
		for (int i = 0; i < row; i++)
		{
			for (int j = 0; j < col; j++)
			{
				a[i][j]=sc.nextInt();
			}
		}
		
		for (int i = 0; i < row; i++)
		{
			for (int j = 0; j < col; j++)
			{
				if(a[i][j]%2==0) {
					even++;
				}else {
					odd++;
				}
			}
		}
		System.out.println ("odd count = "+ odd);
		System.out.println ("even count = "+ even);
	}

}
