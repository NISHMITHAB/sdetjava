package Task1;

public class ReverseArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] arr = {10, 20, 30, 40, 50}; 
		int len= arr.length;
		int [] rev= new int[len];
		int j=0;
		for(int i=len-1;i>=0;i--) {
			rev[j]=arr[i];
			j++;
		}
		System.out.print("Reversed array is : ");
		for(int i=0;i<j;i++) {
			System.out.print(rev[i]+" ");
		}
	}

}
