package Task1;

import java.util.Scanner;

public class SwapTwoNumbersWithoutTemp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number of testcases");
		int testcases=sc.nextInt();
		int arr[]=new int[testcases*2];
		System.out.println("Enter"+ testcases + " testcases to swap");
		for (int i=0;i<testcases*2;i++) {
			arr[i]=sc.nextInt();
		}
		for(int i=0;i<testcases*2;i=i+2) {
			int a=arr[i];
			int b=arr[i+1];
			a= a+b;
			b= a-b;
			a= a-b;
			arr[i]=a;
			arr[i+1]=b;
		}
		System.out.println("swapped test cases are");
		for(int i=0;i<testcases*2;i=i+2) {
			System.out.print(arr[i]+" ");
			System.out.println(arr[i+1]);
		}
	}

}
